// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Engine.h"

#include "BehaviorTree/BlackboardComponent.h"

#include "EnnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class IATEST_API AEnnemyAI : public AAIController
{
	GENERATED_BODY()

	UPROPERTY()
		UBlackBoardComponent *BlackboardComp;

	UPROPERTY(transient)
		class UBehaviorTreeComponent* BehaviorComp;

public:

	AEnnemyAI();

	virtual void OnPossess(APawn *InPawn) override;

	uint8 EnnemyKeyID;

};
