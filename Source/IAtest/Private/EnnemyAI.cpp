// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine.h"
#include "EnnemyCharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"

#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "EnnemyAI.h"

AEnnemyAI::AEnnemyAI()
{
	BlackboardComp = CreateDefaultSubobject<UBlackBoardComponent>(TEXT("BlackboardComp"));

	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
}

void AEnnemyAI::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	AEnnemyCharacter *Char = Cast<AEnnemyCharacter>(InPawn);

	if (Char && Char->BotBehavior)
	{
		BlackboardComp->InitilizeBlackboard(*Char->BotBehavior->BlackboardAsset);
	}
}