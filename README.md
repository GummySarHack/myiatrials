I've submitted my first version and I already had much content started. This project was made using Unreal Engine - C++ - Thrid Person 

    - I've made multiple AIs 
    - Through an algorithm I've made them able to follow me 
        - I have a behaviour tree that allows certain states
        - Made a black board
    - I've made my player character the target and set it as the target for my AIs
    - Added an Unreal Package to have animals and added a wolf
    - Set a state machine for my wolf
        - This allowed me to switch between an idle state and a running state
        - Use animations blueprint for my wolf
            - My wolf remains idle whilst he can't see me (the target)
            - Used a Pawn Sensing component; allows my wolf to see the target within a range and act accordingly 
            - When I get within rnage of my Pawn Sensing, the wolf runs after me